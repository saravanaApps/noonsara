package com.saravana.noon.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.saravana.noon.application.NoonApp;
import com.saravana.noon.model.datamodel.ContentDetails;
import com.saravana.noon.model.datamodel.SearchItem;
import com.saravana.noon.model.datamodel.SearchItemResponse;
import com.saravana.noon.model.datamodel.SearchReq;
import com.saravana.noon.model.network.Resource;
import com.saravana.noon.model.repository.AppRepo;

public class AppVM extends BaseViewModel {

    public AppVM(@NonNull Application application, String key) {
        super(application);
    }

    public static AppVM getViewModel(AppCompatActivity activity, String key) {
        AppVM.Factory factory = new AppVM.Factory(key);
        return ViewModelProviders.of(activity, factory).get(AppVM.class);
    }

    public static AppVM getViewModel(Fragment fragment, String key) {
        AppVM.Factory factory = new AppVM.Factory(key);
        return ViewModelProviders.of(fragment, factory).get(AppVM.class);
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {

        private final String mKey;

        public Factory(String key) {
            this.mKey = key;
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            return (T) new AppVM(NoonApp.getAppContext(), mKey);
        }
    }

    private LiveData<Resource<SearchItemResponse>> mSearchRes;
    private MutableLiveData<SearchReq> mSearchReq = new MutableLiveData<>();

    private LiveData<Resource<ContentDetails>> mDetailsRes;
    private MutableLiveData<SearchReq> mDetailsReq = new MutableLiveData<>();

    private MutableLiveData<SearchItem> selectedSearchItem = new MutableLiveData<>();

    public LiveData<Resource<SearchItemResponse>> getSearchResults() {
        if (mSearchRes == null) {
            mSearchRes = Transformations.switchMap(mSearchReq,
                    input -> AppRepo.getInstance().getSearchResult(input));
        }
        return mSearchRes;
    }

    public void setSearchRequest(SearchReq req) {
        if (req == null) {
            return;
        }
        mSearchReq.setValue(req);
    }

    public LiveData<Resource<ContentDetails>> getDetailResults() {
        if (mDetailsRes == null) {
            mDetailsRes = Transformations.switchMap(mDetailsReq,
                    input -> AppRepo.getInstance().getDetailsResult(input));
        }
        return mDetailsRes;
    }

    public void setDetailRequest(SearchReq req) {
        if (req == null) {
            return;
        }
        mDetailsReq.setValue(req);
    }

    public void setSelectedSearchItem(SearchItem selectedSearchItem) {
        this.selectedSearchItem.setValue(selectedSearchItem);
    }

    public MutableLiveData<SearchItem> getSelectedSearchItem() {
        return selectedSearchItem;
    }

}
