package com.saravana.noon.model.repository;

import android.text.TextUtils;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.saravana.noon.application.NoonApp;
import com.saravana.noon.model.dao.SearchDao;
import com.saravana.noon.model.datamodel.SearchItem;
import com.saravana.noon.model.db.DBHelper;
import com.saravana.noon.utils.AppExecutors;
import com.saravana.noon.utils.NoonLifeCycleOwner;

import java.util.List;

public class BookMarkRepo extends BaseRepo {

    private static BookMarkRepo mInstance;
    private SearchDao searchDao;
    private MutableLiveData<List<String>> searchItemIdsLiveData = null;

    private BookMarkRepo() {
        searchDao = DBHelper.getInstance(NoonApp.getAppContext()).getDB().searchDao();
        searchItemIdsLiveData = new MutableLiveData<>();
    }

    public static synchronized BookMarkRepo getInstance() {
        if (mInstance == null) {
            synchronized (BookMarkRepo.class) {
                if (mInstance == null) {
                    mInstance = new BookMarkRepo();
                }
            }
        }
        return mInstance;
    }

    public LiveData<List<SearchItem>> readAllData() {
        return searchDao.getAll();
    }

    public LiveData<List<String>> readAllIds() {
        return searchItemIdsLiveData;
    }

    public List<String> getSearchItemIds() {
        return searchItemIdsLiveData.getValue();
    }

    public void readAllIds(NoonLifeCycleOwner owner) {
        if (owner != null) {
            searchDao.getAllBookMarkId().observe(owner, strings -> {
                try {
                    searchItemIdsLiveData.setValue(strings);
                } catch (Exception e) {
                    searchItemIdsLiveData.postValue(strings);
                }
            });
        }
    }

    public void addItem(SearchItem searchItem) {
        AppExecutors.getExecutors().diskIO().execute(() -> {
            try {
                if (searchItem == null || TextUtils.isEmpty(searchItem.getImdbID())) {
                    return;
                }
                searchDao.insertAll(searchItem);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public void removeItem(SearchItem searchItem) {
        AppExecutors.getExecutors().diskIO().execute(() -> {
            try {
                if (searchItem == null || TextUtils.isEmpty(searchItem.getImdbID())) {
                    return;
                }
                searchDao.delete(searchItem);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public void deleteAll() {
        AppExecutors.getExecutors().diskIO().execute(() -> {
            searchDao.deleteAll();
            try {
                searchItemIdsLiveData.setValue(null);
            } catch (Exception e) {
                searchItemIdsLiveData.postValue(null);
            }
        });
    }

    public boolean isContainBookMark(SearchItem searchItem) {
        if (getSearchItemIds() == null || searchItem == null || TextUtils.isEmpty(searchItem.getImdbID())) {
            return false;
        }
        return getSearchItemIds().contains(searchItem.getImdbID());
    }

    public boolean isContainBookMark(String id) {
        if (getSearchItemIds() == null || TextUtils.isEmpty(id)) {
            return false;
        }
        return getSearchItemIds().contains(id);
    }

}
