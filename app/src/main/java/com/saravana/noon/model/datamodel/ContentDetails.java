package com.saravana.noon.model.datamodel;

import android.text.TextUtils;

import com.saravana.noon.R;
import com.saravana.noon.application.NoonApp;
import com.saravana.noon.constants.NoonConstant;

import java.util.LinkedHashMap;
import java.util.List;

public class ContentDetails extends BaseDataModel {

    private String Metascore;

    private String BoxOffice;

    private String Website;

    private String imdbRating;

    private String imdbVotes;

    private List<Ratings> Ratings;

    private String Runtime;

    private String Language;

    private String Rated;

    private String Production;

    private String Released;

    private String imdbID;

    private String Plot;

    private String Director;

    private String Title;

    private String Actors;

    private String Type;

    private String Awards;

    private String DVD;

    private String Year;

    private String Poster;

    private String Country;

    private String Genre;

    private String Writer;

    private LinkedHashMap<String, String> moreInformation;

    public String getMetascore() {
        return Metascore;
    }

    public String getBoxOffice() {
        return BoxOffice;
    }

    public String getWebsite() {
        return Website;
    }

    public String getImdbRating() {
        return imdbRating;
    }

    public String getImdbVotes() {
        return imdbVotes;
    }

    public List<Ratings> getRatings() {
        return Ratings;
    }

    public String getRuntime() {
        return TextUtils.isEmpty(Runtime) ? NoonConstant.EMPTY : Runtime;
    }

    public String getLanguage() {
        return Language;
    }

    public String getRated() {
        return Rated;
    }

    public String getProduction() {
        return Production;
    }

    public String getReleased() {
        return Released;
    }

    public String getImdbID() {
        return imdbID;
    }

    public String getPlot() {
        return TextUtils.isEmpty(Plot) ? NoonApp.getAppContext().getString(R.string.not_available) : Plot;
    }

    public String getDirector() {
        return TextUtils.isEmpty(Director) ? NoonApp.getAppContext().getString(R.string.not_available) : Director;
    }

    public String getTitle() {
        return TextUtils.isEmpty(Title) ? NoonApp.getAppContext().getString(R.string.not_available) : Title;
    }

    public String getActors() {
        return TextUtils.isEmpty(Actors) ? NoonApp.getAppContext().getString(R.string.not_available) : Actors;
    }

    public String getType() {
        return TextUtils.isEmpty(Type) ? NoonConstant.EMPTY : Type;
    }

    public String getAwards() {
        return Awards;
    }

    public String getDVD() {
        return DVD;
    }

    public String getYear() {
        return Year;
    }

    public String getPoster() {
        return Poster;
    }

    public String getCountry() {
        return Country;
    }

    public String getGenre() {
        return Genre;
    }

    public String getWriter() {
        return Writer;
    }

    public void parseData() {

        moreInformation = new LinkedHashMap<>();

        if (!TextUtils.isEmpty(getGenre())) {
            moreInformation.put(NoonApp.getAppContext().getString(R.string.genre), getGenre());
        }

        if (!TextUtils.isEmpty(getReleased())) {
            moreInformation.put(NoonApp.getAppContext().getString(R.string.released), getReleased());
        }

        if (!TextUtils.isEmpty(getWriter())) {
            moreInformation.put(NoonApp.getAppContext().getString(R.string.writer), getWriter());
        }

        if (!TextUtils.isEmpty(getLanguage())) {
            moreInformation.put(NoonApp.getAppContext().getString(R.string.language), getLanguage());
        }

        if (!TextUtils.isEmpty(getCountry())) {
            moreInformation.put(NoonApp.getAppContext().getString(R.string.country), getCountry());
        }

        if (!TextUtils.isEmpty(getMetascore())) {
            moreInformation.put(NoonApp.getAppContext().getString(R.string.meta_score), getMetascore());
        }

        if (!TextUtils.isEmpty(getRated())) {
            moreInformation.put(NoonApp.getAppContext().getString(R.string.rated), getRated());
        }

        if (!TextUtils.isEmpty(getAwards())) {
            moreInformation.put(NoonApp.getAppContext().getString(R.string.awards), getAwards());
        }

        if (!TextUtils.isEmpty(getBoxOffice())) {
            moreInformation.put(NoonApp.getAppContext().getString(R.string.boxoffice), getBoxOffice());
        }

        if (!TextUtils.isEmpty(getProduction())) {
            moreInformation.put(NoonApp.getAppContext().getString(R.string.production), getProduction());
        }

        if (!TextUtils.isEmpty(getWebsite())) {
            moreInformation.put(NoonApp.getAppContext().getString(R.string.website), getWebsite());
        }

    }

    public String getImdbRatingVoting() {

        if (!TextUtils.isEmpty(getImdbRating()) && !TextUtils.isEmpty(getImdbVotes())) {
            return NoonApp.getAppContext().getString(R.string.two_value, getImdbRating(), getImdbVotes());
        } else if (!TextUtils.isEmpty(getImdbRating())) {
            return getImdbRating();
        } else if (!TextUtils.isEmpty(getImdbVotes())) {
            return getImdbVotes();
        }

        return NoonApp.getAppContext().getString(R.string.not_available);

    }

    public LinkedHashMap<String, String> getMoreInformation() {
        return moreInformation;
    }

    public String getTypeDuration() {
        return NoonApp.getAppContext().getString(R.string.two_value, getType().toUpperCase(), getRuntime());
    }

    public SearchItem getSearchItemObj() {
        SearchItem searchItem = new SearchItem();
        searchItem.setTitle(getTitle());
        searchItem.setImdbID(getImdbID());
        searchItem.setPoster(getPoster());
        searchItem.setType(getType());
        return searchItem;
    }

}
