package com.saravana.noon.model.repository;

import android.text.TextUtils;
import android.util.LruCache;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.saravana.noon.model.datamodel.ContentDetails;
import com.saravana.noon.model.datamodel.SearchItemResponse;
import com.saravana.noon.model.datamodel.SearchReq;
import com.saravana.noon.model.network.NetworkDBProcessor;
import com.saravana.noon.model.network.Resource;
import com.saravana.noon.model.network.RestClient;

import retrofit2.Call;
import retrofit2.Response;

public class AppRepo extends BaseRepo {

    private static AppRepo sInstance;

    private LruCache<String, SearchItemResponse> responseLruCache;

    private LruCache<String, ContentDetails> detailLruCache;

    private SearchItemResponse cachedDataInMemory;

    private AppRepo() {

        super();

        // We have to set size based on system memory and app allocated memory. Less Time - Now Hardcoded.
        responseLruCache = new LruCache<>(10);

        // We have to set size based on system memory and app allocated memory. Less Time - Now Hardcoded.
        detailLruCache = new LruCache<>(10);

    }

    public static AppRepo getInstance() {
        if (sInstance == null) {
            synchronized (AppRepo.class) {
                if (sInstance == null) {
                    sInstance = new AppRepo();
                }
            }
        }
        return sInstance;
    }


    public LiveData<Resource<SearchItemResponse>> getSearchResult(SearchReq searchReq) {

        if (searchReq == null) {
            return null;
        }

        searchReq.generateURL();

        return new NetworkDBProcessor<SearchItemResponse, SearchItemResponse>() {
            @NonNull
            @Override
            protected Call<SearchItemResponse> createCall() {
                return RestClient.getAPIInterface().searchResult(searchReq.getUrl());
            }

            @Override
            protected boolean shouldFetch(@Nullable SearchItemResponse data) {
                return !responseLruCache.snapshot().containsKey(searchReq.getUrl());
            }

            @Override
            protected SearchItemResponse processResponse(Response<SearchItemResponse> response) {

                if (response != null && response.isSuccessful() && response.body() != null) {

                    if (cachedDataInMemory == null) {
                        cachedDataInMemory = response.body();
                    }

                    if (response.body().isSuccess()) {
                        cachedDataInMemory.parseData(searchReq, response.body());
                    } else {
                        cachedDataInMemory = response.body();
                        cachedDataInMemory.parseData(searchReq, null);
                    }

                }

                return super.processResponse(response);

            }

            @Override
            protected SearchItemResponse saveCallResult(@NonNull SearchItemResponse item) {
                if (searchReq.getPage() <= 1 && item.isSuccess()) {
                    responseLruCache.put(searchReq.getUrl(), item);
                }
                return item;
            }

            @NonNull
            @Override
            protected LiveData<SearchItemResponse> loadFromDb() {

                MutableLiveData<SearchItemResponse> data = new MutableLiveData<>();

                //Validate Search Query
                if (cachedDataInMemory == null || TextUtils.isEmpty(cachedDataInMemory.getSearchQuery()) ||
                        !cachedDataInMemory.getSearchQuery().equals(searchReq.getSearchQuery())) {
                    cachedDataInMemory = null;
                }

                //Load Data from In-Memory Cache
                if (responseLruCache.snapshot().containsKey(searchReq.getUrl())) {
                    cachedDataInMemory = responseLruCache.get(searchReq.getUrl());
                }

                if (cachedDataInMemory != null) {
                    cachedDataInMemory.parseData(searchReq, null);
                }

                data.setValue(cachedDataInMemory);

                return data;

            }

            @Override
            protected void onFetchFailed() {
                super.onFetchFailed();
            }

        }.getAsLiveData();

    }

    public LiveData<Resource<ContentDetails>> getDetailsResult(SearchReq searchReq) {

        if (searchReq == null) {
            return null;
        }

        searchReq.generateURL();

        return new NetworkDBProcessor<ContentDetails, ContentDetails>() {
            @NonNull
            @Override
            protected Call<ContentDetails> createCall() {
                return RestClient.getAPIInterface().detailPage(searchReq.getUrl());
            }

            @Override
            protected boolean shouldFetch(@Nullable ContentDetails data) {
                return !detailLruCache.snapshot().containsKey(searchReq.getUrl());
            }

            @Override
            protected ContentDetails processResponse(Response<ContentDetails> response) {
                if (response != null && response.isSuccessful() && response.body() != null) {
                    response.body().parseData();
                }
                return super.processResponse(response);
            }

            @Override
            protected ContentDetails saveCallResult(@NonNull ContentDetails item) {
                if (item.isSuccess()) {
                    detailLruCache.put(searchReq.getUrl(), item);
                }
                return item;
            }

            @NonNull
            @Override
            protected LiveData<ContentDetails> loadFromDb() {

                MutableLiveData<ContentDetails> data = new MutableLiveData<>();

                //Load Data from In-Memory Cache
                if (detailLruCache.snapshot().containsKey(searchReq.getUrl())) {
                    data.setValue(detailLruCache.get(searchReq.getUrl()));
                } else {
                    data.setValue(null);
                }

                return data;

            }

            @Override
            protected void onFetchFailed() {
                super.onFetchFailed();
            }

        }.getAsLiveData();

    }

}

