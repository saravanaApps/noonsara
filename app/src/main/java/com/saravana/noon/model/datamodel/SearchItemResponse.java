package com.saravana.noon.model.datamodel;

import android.text.TextUtils;

import com.saravana.noon.utils.ViewUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class SearchItemResponse extends BaseDataModel {

    private int pageNo;

    private String searchQuery;

    private String totalResults;

    private boolean isRecordSetOver;

    private int viewType;

    private List<SearchItem> Search;

    public List<SearchItem> getSearch() {
        return Search;
    }

    public void setSearch(List<SearchItem> search) {
        Search = search;
    }

    public int getTotalResults() {
        if (TextUtils.isEmpty(totalResults) || !TextUtils.isDigitsOnly(totalResults)) {
            return 0;
        }
        return Integer.valueOf(totalResults);
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }

    public void setTotalResults(String totalResults) {
        this.totalResults = totalResults;
    }

    public void setRecordSetOver(boolean recordSetOver) {
        isRecordSetOver = recordSetOver;
    }

    public boolean isRecordSetOver() {
        return isRecordSetOver;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public int getViewType() {
        return viewType;
    }

    public void parseData(SearchReq searchReq, SearchItemResponse response) {

        if (searchReq == null) {
            return;
        }

        setPageNo(searchReq.getPage());
        setSearchQuery(searchReq.getSearchQuery());
        setViewType(searchReq.getViewType());

        if (getViewType() == ViewUtils.VIEW_SEARCH_SUGGESTION) {
            setRecordSetOver(true);
            return;
        }

        if (!isSuccess()) {
            return;
        }

        int currentRecords = searchReq.getPage() * searchReq.getPageSize();

        setRecordSetOver((currentRecords >= getTotalResults()));

        if (getSearch() == null || getSearch().isEmpty()) {
            return;
        }

        //Removing Loader Object
        removeLoaderData();

        if (pageNo > 1 && response != null && response.getSearch() != null) {
            getSearch().addAll(response.getSearch());
        }

        if (isRecordSetOver()) {
            return;
        }

        getSearch().addAll(getLoaderData(4));

    }

    private void removeLoaderData() {

        try {

            final ListIterator<SearchItem> iterator = getSearch().listIterator(getSearch().size());

            while (iterator.hasPrevious()) {
                SearchItem item = iterator.previous();
                if (item != null && item.getViewType() == ViewUtils.VIEW_LOADER) {
                    iterator.remove();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private ArrayList<SearchItem> getLoaderData(int count) {

        ArrayList<SearchItem> list = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            SearchItem searchItem = new SearchItem();
            searchItem.setViewType(ViewUtils.VIEW_LOADER);
            list.add(searchItem);
        }

        return list;

    }

    public String getTotalResultsInStr() {
        return totalResults;
    }
}

