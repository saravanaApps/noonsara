package com.saravana.noon.model.network;

public class RestClientImpl {

    private RestInterface mRestInterface;

    private static RestClientImpl mRestImpl;

    private RestClientImpl(RestInterface restInterface) {
        mRestInterface = restInterface;
    }

    public static RestClientImpl getInstance() {
        if (mRestImpl == null) {
            synchronized (RestClientImpl.class) {
                if (mRestImpl == null) {
                    mRestImpl = new RestClientImpl(RestClient.getAPIInterface());
                }
            }
        }
        return mRestImpl;
    }

}
