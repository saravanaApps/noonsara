package com.saravana.noon.model.network;

import com.saravana.noon.model.datamodel.BaseDataModel;
import com.saravana.noon.model.datamodel.ContentDetails;
import com.saravana.noon.model.datamodel.SearchItemResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface RestInterface {

    @GET
    Call<SearchItemResponse> searchResult(@Url String url);

    @GET
    Call<ContentDetails> detailPage(@Url String url);

}
