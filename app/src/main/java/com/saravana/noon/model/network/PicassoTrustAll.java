package com.saravana.noon.model.network;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;

import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.saravana.noon.BuildConfig;
import com.saravana.noon.R;
import com.saravana.noon.application.NoonApp;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.Serializable;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;


public class PicassoTrustAll implements Serializable {

    private static Picasso mInstance = null;
    private static volatile PicassoTrustAll picassoTrustAll = null;

    private PicassoTrustAll(Context context) {
        try {
            mInstance = new Picasso.Builder(context)
                    .downloader(new OkHttp3Downloader(RestClient.getSSLClientForImage()))
                    .indicatorsEnabled(BuildConfig.DEBUG)
                    .listener((picasso, uri, exception) -> {
                        Log.d(PicassoTrustAll.class.getSimpleName(), "Image Error : " + exception.toString() + " : " +
                                uri.toString());
                    }).build();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static PicassoTrustAll getInstance(Context context) {
        if (picassoTrustAll == null || mInstance == null) {
            synchronized (PicassoTrustAll.class) {
                if (picassoTrustAll == null || mInstance == null) {
                    picassoTrustAll = new PicassoTrustAll(context);
                }
            }
        }
        return picassoTrustAll;
    }

    public void loadImage(final ImageView imgProduct, String imageURL, Context mContext) {

        if (TextUtils.isEmpty(imageURL)) {
            setPlaceHolderImage(imgProduct, mContext);
            return;
        }

        if (imageURL == null) {
            return;
        }

        mInstance.load(getImageUrl(imageURL))
                .error(VectorDrawableCompat.create(mContext.getResources(), R.drawable.vec_placeholder, null))
                .placeholder(VectorDrawableCompat.create(mContext.getResources(), R.drawable.vec_placeholder, null))
                .into(imgProduct, new Callback() {
                    @Override
                    public void onSuccess() {
                        imgProduct.setScaleType(ImageView.ScaleType.FIT_XY);
                    }

                    @Override
                    public void onError() {
                        imgProduct.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    }
                });

    }

    private String getImageUrl(String imageURL) {
        StringBuilder url = new StringBuilder(imageURL.split("\\|")[0]);
        return url.toString();
    }

    public void setPlaceHolderImage(ImageView imgProduct, Context context) {
        try {
            imgProduct.setScaleType(ImageView.ScaleType.FIT_CENTER);
            mInstance.load("NO-Image")
                    .fit()
                    .error(VectorDrawableCompat.create(context.getResources(),
                            R.drawable.vec_placeholder, null))
                    .placeholder(VectorDrawableCompat.create(context.getResources(),
                            R.drawable.vec_placeholder, null))
                    .into(imgProduct);
        } catch (Exception e) {
        }
    }

    protected PicassoTrustAll readResolve() {
        return getInstance(NoonApp.getAppContext());
    }

}
