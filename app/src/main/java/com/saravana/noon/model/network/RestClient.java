package com.saravana.noon.model.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ihsanbal.logging.Level;
import com.saravana.noon.BuildConfig;
import com.saravana.noon.application.NoonApp;
import com.saravana.noon.constants.NoonConstant;

import java.io.File;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Cache;
import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.internal.platform.Platform;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {

    private final static int cacheSize = 10 * 1024 * 1024; // 10 MiB
    private static X509TrustManager trustManager = null;
    private static Retrofit retrofit = null;
    private static RestInterface restInterface;

    public static RestInterface getAPIInterface() {
        if (restInterface == null) {
            restInterface = getClient().create(RestInterface.class);
        }
        return restInterface;
    }

    public static Retrofit getClient() {
        if (retrofit == null) {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.SERVER_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(getRequestHeader())
                    .build();
        }
        return retrofit;
    }

    private static OkHttpClient getRequestHeader() {
        OkHttpClient okHttpClient = null;
        try {
            okHttpClient = getSSLClient().newBuilder()
                    .cache(new Cache(NoonApp.getAppContext().getCacheDir(), cacheSize))
                    .build();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return okHttpClient;
    }


    public static SSLSocketFactory getUnsafeSSL() {
        try {
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, getTrustManager(), new java.security.SecureRandom());
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
            return sslSocketFactory;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static TrustManager[] getTrustManager() {
        final TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                                                   String authType) {
                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                                                   String authType) {
                    }

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return new java.security.cert.X509Certificate[0];
                    }
                }
        };
        return trustAllCerts;
    }

    private static X509TrustManager getX509TrustManager() {
        if (trustManager == null) {
            trustManager = new X509TrustManager() {
                @Override
                public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType)
                        throws CertificateException {
                }

                @Override
                public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType)
                        throws CertificateException {
                }

                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[]{};
                }
            };
        }
        return trustManager;
    }

    public static OkHttpClient getSSLClient() throws NoSuchAlgorithmException,
            KeyStoreException, KeyManagementException, CertificateException, IOException {

        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        File httpCacheDirectory = new File(NoonApp.getAppContext().getCacheDir(), "responses");
        Cache cache = new Cache(httpCacheDirectory, cacheSize);
        builder.cache(cache);

        builder.readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .connectTimeout(1, TimeUnit.MINUTES)
                .connectionPool(new ConnectionPool(100, 5, TimeUnit.MINUTES))
                .retryOnConnectionFailure(true)
                .authenticator(new SessionIdAuthenticator());

        builder.addInterceptor(new HeaderInterceptor());
        builder.addInterceptor(new ErrorHandlingInterceptor());

        if (BuildConfig.DEBUG) {
            builder.addInterceptor(new com.ihsanbal.logging.LoggingInterceptor.Builder()
                    .loggable(BuildConfig.DEBUG)
                    .setLevel(Level.BASIC)
                    .log(Platform.INFO)
                    .request("Request")
                    .response("Response")
                    .build());
        }

        if (BuildConfig.FLAVOR.equalsIgnoreCase(NoonConstant.PRO)) {
            builder.sslSocketFactory(new TLSSocket(NoonApp.getAppContext()), getX509TrustManager());
        } else {
            builder.sslSocketFactory(getUnsafeSSL(), getX509TrustManager());
        }

        builder.hostnameVerifier((hostname, session) -> true);

        return builder.build();

    }

    public static OkHttpClient getSSLClientForImage() throws NoSuchAlgorithmException, KeyStoreException,
            KeyManagementException, CertificateException, IOException {

        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        builder.networkInterceptors().add(chain -> {
            if (chain.request().url() != null && chain.request().url().pathSize() < 2) {
                return new Response.Builder().code(503).request(chain.request()).build();
            }
            Response originalResponse = chain.proceed(chain.request());
            return originalResponse.newBuilder().header("Cache-Control", "max-age=" + (60 * 60 * 24)).build();
        });

        builder.cache(new Cache(NoonApp.getAppContext().getCacheDir(), cacheSize));

        builder.readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .connectTimeout(1, TimeUnit.MINUTES)
                .connectionPool(new ConnectionPool(100, 5, TimeUnit.MINUTES))
                .retryOnConnectionFailure(true);

        if (BuildConfig.FLAVOR.equalsIgnoreCase(NoonConstant.PRO)) {
            builder.sslSocketFactory(new TLSSocket(NoonApp.getAppContext()), getX509TrustManager());
        } else {
            builder.sslSocketFactory(getUnsafeSSL(), getX509TrustManager());
        }

        builder.hostnameVerifier((hostname, session) -> true);

        return builder.build();

    }

}