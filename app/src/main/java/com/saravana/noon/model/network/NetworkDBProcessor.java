package com.saravana.noon.model.network;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.saravana.noon.R;
import com.saravana.noon.application.NoonApp;
import com.saravana.noon.model.datamodel.BaseDataModel;
import com.saravana.noon.utils.AppExecutors;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class NetworkDBProcessor<DBResType, APIResType> {

    private final MediatorLiveData<Resource<DBResType>> result = new MediatorLiveData<>();

    protected String mKey = null;
    protected Object object = null;
    private boolean isLoadingRequire = true;
    private DBResType dbResType = null;

    @MainThread
    public NetworkDBProcessor() {
        performCall();
    }

    @MainThread
    public NetworkDBProcessor(Object object, boolean isLoadingRequire) {
        if (object instanceof String) {
            mKey = (String) object;
        } else {
            setObject(object);
            setKey();
        }
        this.isLoadingRequire = isLoadingRequire;
        performCall();
    }

    private void performCall() {
        if (isLoadingRequire) {
            result.setValue(Resource.loading(null));
        }
        LiveData<DBResType> dbSource = loadFromDb();
        result.addSource(dbSource, data -> {
            result.removeSource(dbSource);
            if (shouldFetch(data)) {
                fetchFromNetwork(dbSource);
            } else {
                result.addSource(dbSource, newData -> NetworkDBProcessor.this.setValue(Resource.success(newData)));
            }
        });
    }

    private void fetchFromNetwork(final LiveData<DBResType> dbSource) {

        if (isLoadingRequire) {
            result.addSource(dbSource, newData -> {
                result.removeSource(dbSource);
                result.setValue(Resource.loading(newData));
            });
        }

        createCall().enqueue(new Callback<APIResType>() {
            @Override
            public void onResponse(Call<APIResType> call, Response<APIResType> response) {
                result.removeSource(dbSource);
                processSaveAndPost(response);
            }

            @Override
            public void onFailure(Call<APIResType> call, Throwable t) {

                onFetchFailed();

                result.removeSource(dbSource);

                AppExecutors.getExecutors().mainThread().execute(() -> {
                    if (t instanceof IOException) {
                        result.addSource(dbSource, newData -> setValue(Resource.error(NoonApp.getAppContext().getString(R.string.default_error),
                                newData)));
                    } else {
                        result.addSource(dbSource, newData -> setValue(Resource.error(t.getMessage(), newData)));
                    }
                });

            }
        });

    }

    @MainThread
    private void processSaveAndPost(Response<APIResType> response) {

        AppExecutors.getExecutors().networkIO().execute(() -> {

            dbResType = saveCallResult(processResponse(response));

            try {
                if (dbResType == null && !response.isSuccessful() && response.errorBody() != null) {
                    dbResType = BaseDataModel.getErrorResponseBody(getClass(), response);
                } else if (dbResType == null && response.body() != null) {
                    dbResType = (DBResType) response.body();
                }
            } catch (Exception e) {
            }

            AppExecutors.getExecutors().mainThread().execute(() -> {

                if (dbResType != null && dbResType instanceof BaseDataModel) {

                    BaseDataModel baseDataModel = (BaseDataModel) dbResType;
                    baseDataModel.setHttpStatusCode(response.code());

                    if (!baseDataModel.isSuccess()) {
                        NetworkDBProcessor.this.setValue(Resource.error(baseDataModel.getMessage(), dbResType));
                        return;
                    }

                }

                result.addSource(loadFromDb(), newData -> setValue(Resource.success(newData)));

            });
        });

    }

    @WorkerThread
    protected DBResType processResponse(Response<APIResType> response) {
        if (response != null) {
            return (DBResType) response.body();
        }
        return null;
    }

    @WorkerThread
    protected abstract DBResType saveCallResult(@NonNull DBResType item);

    @MainThread
    protected boolean shouldFetch(@Nullable DBResType data) {
        return true;
    }

    @NonNull
    @MainThread
    protected abstract LiveData<DBResType> loadFromDb();

    @NonNull
    @MainThread
    protected abstract Call<APIResType> createCall();

    @MainThread
    protected void onFetchFailed() {
    }

    public final LiveData<Resource<DBResType>> getAsLiveData() {
        return result;
    }

    @MainThread
    public void setValue(Resource<DBResType> newValue) {
        try {
            if (newValue != null && newValue.data != null && newValue.data instanceof BaseDataModel) {
                ((BaseDataModel) newValue.data).setRequestObject(object);
            }
        } catch (Exception e) {
        }

        if (result.getValue() != newValue) {
            result.setValue(newValue);
        }
    }

    public void setObject(Object object) {
        this.object = object;
    }

    protected void setKey() {
    }

}