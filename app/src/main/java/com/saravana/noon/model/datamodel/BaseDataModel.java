package com.saravana.noon.model.datamodel;

import android.text.TextUtils;

import com.saravana.noon.constants.NoonConstant;
import com.saravana.noon.model.network.RestClient;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

public class BaseDataModel {

    protected Object requestObject;

    protected String Response;

    protected String Error;

    protected int statusCode;

    public void setHttpStatusCode(int code) {
        statusCode = code;
    }

    public void setError(String error) {
        this.Error = error;
    }

    public boolean isSuccess() {
        return (!TextUtils.isEmpty(Response) && (Response.equals("True")));
    }

    public String getMessage() {
        return Error;
    }

    public static <ResType, APIResType> ResType getErrorResponseBody(Class<?> className, Response<APIResType> response) {

        ResType viewResType = null;

        try {

            String name = ((ParameterizedType) className.getGenericSuperclass()).getActualTypeArguments()[0].toString();

            viewResType = (ResType) Class.forName(name.replace("class ", NoonConstant.EMPTY)).getConstructors()[0].newInstance();

            Converter<ResponseBody, BaseDataModel> converter = RestClient.getClient().responseBodyConverter(BaseDataModel.class, new Annotation[0]);
            BaseDataModel baseDataModel = converter.convert(response.errorBody());

            if (viewResType != null && baseDataModel != null && viewResType instanceof BaseDataModel) {
            }

        } catch (Exception e) {
            e.printStackTrace();
        } catch (Error error) {
            error.printStackTrace();
        }

        return viewResType;

    }

    public void setRequestObject(Object request) {
        this.requestObject = request;
    }

    public Object getRequestObject() {
        return requestObject;
    }

}
