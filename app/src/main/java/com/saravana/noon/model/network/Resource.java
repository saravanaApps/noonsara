package com.saravana.noon.model.network;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class Resource<T> {

    @NonNull
    public final APIStatus status;
    @Nullable
    public final T data;
    @Nullable
    public final String message;

    private Resource(@NonNull APIStatus status, @Nullable T data,
                     @Nullable String message) {
        this.status = status;
        this.data = data;
        this.message = message;
    }

    public static <T> Resource<T> success(@NonNull T data) {
        return new Resource<>(APIStatus.SUCCESS, data, null);
    }

    public static <T> Resource<T> sessionExpired(String msg, @Nullable T data) {
        return new Resource<>(APIStatus.SESSION_EXPIRED, data, msg);
    }

    public static <T> Resource<T> error(String msg, @Nullable T data) {
        return new Resource<>(APIStatus.ERROR, data, msg);
    }

    public static <T> Resource<T> loading(@Nullable T data) {
        return new Resource<>(APIStatus.LOADING, data, null);
    }

    public enum APIStatus {SUCCESS, ERROR, LOADING, SESSION_EXPIRED}

}
