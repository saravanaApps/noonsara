package com.saravana.noon.model.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.saravana.noon.model.dao.SearchDao;
import com.saravana.noon.model.datamodel.SearchItem;

@Database(entities = {SearchItem.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract SearchDao searchDao();
}