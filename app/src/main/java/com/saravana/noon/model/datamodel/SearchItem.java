package com.saravana.noon.model.datamodel;

import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.saravana.noon.constants.NoonConstant;
import com.saravana.noon.utils.ViewUtils;

@Entity(tableName = "searchitem", indices = {@Index("id")})
public class SearchItem {

    @PrimaryKey
    @ColumnInfo(name = "id")
    @NonNull
    private String imdbID;

    @ColumnInfo(name = "type")
    private String Type;

    @ColumnInfo(name = "year")
    private String Year;

    @ColumnInfo(name = "poster")
    private String Poster;

    @ColumnInfo(name = "title")
    private String Title;

    //Local App Properties

    private int viewType;

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String Year) {
        this.Year = Year;
    }

    public String getImdbID() {
        return TextUtils.isEmpty(imdbID) ? NoonConstant.EMPTY : imdbID;
    }

    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }

    public String getPoster() {
        return Poster;
    }

    public void setPoster(String Poster) {
        this.Poster = Poster;
    }

    public String getTitle() {
        return TextUtils.isEmpty(Title) ? NoonConstant.EMPTY : Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public int getViewType() {
        return viewType <= 0 ? ViewUtils.VIEW_SEARCH_ITEM : viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

}
