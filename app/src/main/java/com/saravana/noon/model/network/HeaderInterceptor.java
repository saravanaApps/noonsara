package com.saravana.noon.model.network;

import com.saravana.noon.constants.ApiConstants;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class HeaderInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {

        Request original = chain.request();

        Request.Builder builder = original.newBuilder();

        /* We can add common our application rest api level header properties */
        builder.addHeader(ApiConstants.CONTENT_TYPE, ApiConstants.APPLICATION_JSON);
        builder.addHeader(ApiConstants.ACCEPT_HEADER_KEY, ApiConstants.APPLICATION_JSON);

        return chain.proceed(builder.build());
    }
}
