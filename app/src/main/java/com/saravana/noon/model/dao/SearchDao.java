package com.saravana.noon.model.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.saravana.noon.model.datamodel.SearchItem;

import java.util.List;

@Dao
public interface SearchDao {

    @Query("SELECT * FROM searchitem")
    LiveData<List<SearchItem>> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(SearchItem... users);

    @Delete
    void delete(SearchItem user);

    @Query("DELETE FROM searchitem")
    void deleteAll();

    @Query("SELECT id FROM searchitem")
    LiveData<List<String>> getAllBookMarkId();
}
