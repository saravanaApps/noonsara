package com.saravana.noon.model.network;

import android.util.Log;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.saravana.noon.R;
import com.saravana.noon.application.NoonApp;
import com.saravana.noon.model.datamodel.BaseDataModel;
import com.saravana.noon.utils.AppExecutors;
import com.saravana.noon.constants.NoonConstant;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class NetworkProcessor<ResType, APIResType> {

    protected final MutableLiveData<Resource<ResType>> result = new MutableLiveData<>();
    protected String key = NoonConstant.EMPTY;
    protected Object request = null;
    private ResType viewResType;

    @MainThread
    public NetworkProcessor() {
        performNetworkCall();
    }

    @MainThread
    public NetworkProcessor(String key) {
        this.key = key;
        performNetworkCall();
    }

    @MainThread
    public NetworkProcessor(String key, Object request) {
        this.key = key;
        this.request = request;
        performNetworkCall();
    }

    private void performNetworkCall() {
        result.setValue(Resource.loading(null));
        fetchFromNetwork();
    }

    private void fetchFromNetwork() {

        createCall().enqueue(new Callback<APIResType>() {
            @Override
            public void onResponse(Call<APIResType> call, Response<APIResType> response) {
                Log.i(NetworkProcessor.class.getSimpleName(), "NetworkCall : Success : " + call.request().url().toString());
                processSaveAndPost(response);
            }

            @Override
            public void onFailure(Call<APIResType> call, Throwable t) {

                Log.i(NetworkProcessor.class.getSimpleName(), "NetworkCall : Failure : " + call.request().url().toString() + " : " + t.getMessage());

                onFetchFailed();

                AppExecutors.getExecutors().mainThread().execute(() -> {
                    if (t instanceof IOException) {
                        setValue(Resource.error(NoonApp.getAppContext().getString(R.string.default_error), null));
                    } else {
                        setValue(Resource.error(t.getMessage(), null));
                    }
                });

            }

        });

    }

    @MainThread
    private void processSaveAndPost(Response<APIResType> response) {

        AppExecutors.getExecutors().networkIO().execute(() -> {

            //Process Data inside Network Thread
            viewResType = processResponse(response);

            try {
                if (viewResType == null && !response.isSuccessful() && response.errorBody() != null) {
                    viewResType = BaseDataModel.getErrorResponseBody(getClass(), response);
                } else if (viewResType == null && response.body() != null) {
                    viewResType = (ResType) response.body();
                }
            } catch (Exception e) {
            }

            AppExecutors.getExecutors().mainThread().execute(() -> {

                //Post Data inside Main Thread
                if (viewResType != null && viewResType instanceof BaseDataModel) {

                    BaseDataModel baseDataModel = (BaseDataModel) viewResType;
                    baseDataModel.setHttpStatusCode(response.code());

                    if (!baseDataModel.isSuccess()) {
                        NetworkProcessor.this.setValue(Resource.error(baseDataModel.getMessage(), viewResType));
                        return;
                    }
                }

                if (response.isSuccessful()) {
                    NetworkProcessor.this.setValue(Resource.success(viewResType));
                } else {
                    NetworkProcessor.this.setValue(Resource.error(NoonApp.getAppContext().getString(R.string.default_error), viewResType));
                }

            });
        });

    }

    @WorkerThread
    protected ResType processResponse(Response<APIResType> response) {
        if (response != null) {
            return (ResType) response.body();
        }
        return null;
    }

    @NonNull
    @MainThread
    protected abstract Call<APIResType> createCall();

    @MainThread
    protected void onFetchFailed() {
    }

    public final LiveData<Resource<ResType>> getAsLiveData() {
        return result;
    }

    @MainThread
    protected void setValue(Resource<ResType> newValue) {

        try {
            if (newValue != null && newValue.data != null && newValue.data instanceof BaseDataModel) {
                ((BaseDataModel) newValue.data).setRequestObject(request);
            }
        } catch (Exception e) {
        }

        if (result.getValue() != newValue) {
            result.setValue(newValue);
        }
    }

}