package com.saravana.noon.model.datamodel;

import android.text.TextUtils;

import com.saravana.noon.BuildConfig;
import com.saravana.noon.utils.ViewUtils;

public class SearchReq extends BaseRequest {

    private String searchQuery;

    private int page = 1;

    private int pageSize = 10;

    private String imdbId;

    private String url;

    private int viewType = ViewUtils.VIEW_SEARCH_SUGGESTION;

    /**
     * full / short
     */
    private String plot;

    public SearchReq() {

    }

    public SearchReq(String query) {
        setSearchQuery(query);
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public String getPlot() {
        return plot;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public String getImdbId() {
        return imdbId;
    }

    public void setImdbId(String imdbId) {
        this.imdbId = imdbId;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public int getViewType() {
        return viewType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String generateURL() {

        StringBuilder s = new StringBuilder();

        s.append(BuildConfig.SERVER_URL);

        s.append("/?");

        if (!TextUtils.isEmpty(searchQuery)) {
            s.append("s=" + searchQuery + "&");
        }

        if (page > 0) {
            s.append("page=" + page + "&");
        }

        if (!TextUtils.isEmpty(plot)) {
            s.append("plot=" + plot + "&");
        }

        if (!TextUtils.isEmpty(imdbId)) {
            s.append("i=" + imdbId + "&");
        }

        s.append("apikey=" + BuildConfig.API_KEY);

        setUrl(s.toString());

        return getUrl();

    }

}
