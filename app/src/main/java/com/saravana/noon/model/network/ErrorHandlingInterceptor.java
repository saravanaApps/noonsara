package com.saravana.noon.model.network;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class ErrorHandlingInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request original = chain.request();
        Response response = chain.proceed(original);

        boolean badRequest = (response.code() == 400 || response.code() == 403);

        /*
        * We can do application rest api level error - in common place
        * */

        return response;

    }

}
