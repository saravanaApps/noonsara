package com.saravana.noon.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.telephony.TelephonyManager;

import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;

import com.saravana.noon.R;
import com.saravana.noon.application.NoonApp;
import com.saravana.noon.constants.NoonConstant;

public class NetworkUtils extends ConnectivityManager.NetworkCallback {

    private static NetworkUtils networkUtils = null;
    private static Context context;
    private NetworkRequest networkRequest = null;
    private static String networkType = NoonConstant.EMPTY;
    private MutableLiveData<Boolean> networkLiveData = null;

    private NetworkUtils(Context obj) {

        this.context = obj;

        networkLiveData = new MediatorLiveData<>();
        networkLiveData.setValue(isNetworkAvailable());

        try {

            NetworkRequest.Builder builder = new NetworkRequest.Builder();

            addTransportType(builder, NetworkCapabilities.TRANSPORT_CELLULAR);
            addTransportType(builder, NetworkCapabilities.TRANSPORT_WIFI);
            addTransportType(builder, NetworkCapabilities.TRANSPORT_BLUETOOTH);
            addTransportType(builder, NetworkCapabilities.TRANSPORT_ETHERNET);
            addTransportType(builder, NetworkCapabilities.TRANSPORT_VPN);
            addTransportType(builder, NetworkCapabilities.TRANSPORT_WIFI_AWARE);
            addTransportType(builder, NetworkCapabilities.TRANSPORT_LOWPAN);

            networkRequest = builder.build();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * In Few device - few capability not available. Hence adding with Try Catch
     */
    private void addTransportType(NetworkRequest.Builder builder, int capabilities) {
        try {
            builder.addTransportType(capabilities);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static NetworkUtils getInstance(Context obj) {
        if (networkUtils == null) {
            synchronized (NetworkUtils.class) {
                if (networkUtils == null) {
                    networkUtils = new NetworkUtils(obj);
                }
            }
        }
        context = obj;
        return networkUtils;
    }

    @Override
    public void onAvailable(Network network) {
        super.onAvailable(network);
        onNetworkUpdate();
    }

    @Override
    public void onUnavailable() {
        super.onUnavailable();
        onNetworkUpdate();
    }

    @Override
    public void onLost(Network network) {
        super.onLost(network);
        onNetworkUpdate();
    }

    private void onNetworkUpdate() {
        networkType = getNetworkType(context);
        boolean value = isNetworkAvailable();
        try {
            networkLiveData.setValue(value);
        } catch (IllegalStateException e) {
            networkLiveData.postValue(value);
        }
    }

    public void register() {
        try {
            unRegister();
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            connectivityManager.registerNetworkCallback(networkRequest, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void unRegister() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            connectivityManager.unregisterNetworkCallback(this);
        } catch (Exception e) {
        }
    }

    public static String getNetworkType() {
        return networkType;
    }

    public boolean isNetworkAvailable() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return (activeNetworkInfo != null && activeNetworkInfo.isConnected());
        } catch (Exception e) {
        }
        return true;
    }

    private String getNetworkType(Context context) {

        try {

            ConnectivityManager mgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo ni = mgr.getActiveNetworkInfo();

            if (ni == null || !(ni.isConnected())) {
                return NoonConstant.EMPTY;
            }

            switch (ni.getType()) {
                case ConnectivityManager.TYPE_MOBILE:
                    return getMobileNetworkType(context);
                case ConnectivityManager.TYPE_WIFI:
                    return NoonApp.getAppContext().getString(R.string.wifi);
                default:
                    return NoonConstant.EMPTY;
            }
        } catch (Exception e) {
            return NoonConstant.EMPTY;
        }

    }

    private String getMobileNetworkType(Context context) {

        try {
            TelephonyManager mTelephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            int networkType = mTelephonyManager.getNetworkType();

            switch (networkType) {
                case TelephonyManager.NETWORK_TYPE_GPRS:
                case TelephonyManager.NETWORK_TYPE_EDGE:
                case TelephonyManager.NETWORK_TYPE_CDMA:
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                case TelephonyManager.NETWORK_TYPE_IDEN:
                    return NoonApp.getAppContext().getString(R.string.two_g_network);
                case TelephonyManager.NETWORK_TYPE_UMTS:
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                case TelephonyManager.NETWORK_TYPE_HSPA:
                case TelephonyManager.NETWORK_TYPE_EVDO_B:
                case TelephonyManager.NETWORK_TYPE_EHRPD:
                case TelephonyManager.NETWORK_TYPE_HSPAP:
                    return NoonApp.getAppContext().getString(R.string.three_g_network);
                case TelephonyManager.NETWORK_TYPE_LTE:
                    return NoonApp.getAppContext().getString(R.string.four_g_network);
                default:
                    return NoonConstant.EMPTY;
            }
        } catch (Exception e) {
        }

        return NoonConstant.EMPTY;

    }

    public MutableLiveData<Boolean> getNetworkLiveData() {
        return networkLiveData;
    }

}
