
package com.saravana.noon.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.os.Handler;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.saravana.noon.application.NoonApp;
import com.saravana.noon.view.activity.BaseActivity;

import java.util.HashMap;

public class ViewUtils {

    public static final int VIEW_SEARCH_ITEM = 1;
    public static final int VIEW_LOADER = 2;
    public static final int VIEW_FOOTER = 3;
    public static final int VIEW_SEARCH_SUGGESTION = 4;
    public static final int VIEW_SEARCH_RESULTS = 5;
    public static final int VIEW_SEARCH_BOOKMARK = 6;

    private static String displayMetrics = null;
    private static Point displayPoint = null;
    private static HashMap<Float, Integer> dpToPixel = new HashMap<>();

    public static String getDisplayMetrics() {
        try {
            if (TextUtils.isEmpty(displayMetrics)) {
                displayMetrics = String.valueOf(NoonApp.getAppContext().getResources().getDisplayMetrics().densityDpi);
            }
        } catch (Exception e) {
            displayMetrics = "420";
        }
        return displayMetrics;
    }

    public static void updateDisplayPoint(Context context) {

        displayPoint = new Point();

        if (context == null) {
            return;
        }

        try {
            if (context instanceof BaseActivity) {
                ((BaseActivity) context).getWindowManager().getDefaultDisplay().getSize(displayPoint);
            } else if (context instanceof ContextThemeWrapper) {
                displayPoint.x = context.getTheme().getResources().getDisplayMetrics().widthPixels;
                displayPoint.y = context.getTheme().getResources().getDisplayMetrics().heightPixels;
            }
        } catch (Exception e) {
        }

    }

    public static int getColumnPercentagePosition(Context context, int padding, int numberOfColumns) {
        try {
            if (displayPoint == null) {
                updateDisplayPoint(context);
            }
            int width = displayPoint.x - dpToPixel(padding, context);
            width = width / numberOfColumns;
            return width;
        } catch (Exception e) {
        }
        return 0;
    }

    public static Point getPointsAspectRatio(Context context, float imageColumn, int padding, float aspectRatio) {
        Point op = new Point();
        try {
            if (displayPoint == null) {
                updateDisplayPoint(context);
            }
            int width = displayPoint.x - dpToPixel(padding, context);
            int height;
            width = (int) ((width / imageColumn));
            height = (int) (width / aspectRatio);
            op.x = width;
            op.y = height;
        } catch (Exception e) {
        }
        return op;
    }

    public static String dpToPixelString(float dp, Context context) {
        return String.valueOf(dpToPixel(dp, context));
    }

    public static int dpToPixel(float dp, Context context) {
        if (dp <= 0) {
            return 0;
        }
        if (dpToPixel.containsKey(dp)) {
            return dpToPixel.get(dp);
        }
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        dpToPixel.put(dp, (int) (dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_MEDIUM)));
        return dpToPixel.get(dp);
    }


    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    public static void hideSoftKeyboard(View view, Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    public static void showSoftKeyboard(final Context context, final View view, int delay) {
        try {
            new Handler().postDelayed(() -> {
                try {
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_IMPLICIT_ONLY);
                    imm.showSoftInput(view, 0);
                } catch (Exception e) {
                }
            }, delay);
        } catch (Exception e) {
        }
    }

}
