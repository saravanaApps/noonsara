package com.saravana.noon.utils;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import androidx.appcompat.app.AppCompatActivity;

import com.saravana.noon.constants.BundleConstant;
import com.saravana.noon.model.datamodel.SearchItem;
import com.saravana.noon.view.activity.BookMarkActivity;
import com.saravana.noon.view.activity.DetailsActivity;
import com.saravana.noon.view.activity.SearchActivity;
import com.saravana.noon.view.dialog.NetworkDialog;

public class NavUtil {

    private static NavUtil navUtils = null;
    private static Context context;

    private NavUtil(Context obj) {
        this.context = obj;
    }

    public static NavUtil getNavUtils(Context obj) {
        if (navUtils == null) {
            synchronized (NavUtil.class) {
                if (navUtils == null) {
                    navUtils = new NavUtil(obj);
                }
            }
        }
        context = obj;
        return navUtils;
    }

    public void onNetworkStatusChange(AppCompatActivity activity, NetworkDialog networkDialog, boolean isAvailable) {

        try {

            if (isAvailable) {
                return;
            }

            if (networkDialog == null || networkDialog.isRemoving() || networkDialog.isDetached()) {
                networkDialog = NetworkDialog.newInstance();
                networkDialog.show(activity.getSupportFragmentManager(),
                        NetworkDialog.class.getSimpleName());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void openSearchActivity() {
        Intent intent = new Intent(context, SearchActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    public void openBookMarkActivity() {
        Intent intent = new Intent(context, BookMarkActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        context.startActivity(intent);
    }

    public void openDetailsActivity(SearchItem searchItem) {

        if (searchItem == null || TextUtils.isEmpty(searchItem.getImdbID())) {
            return;
        }

        Intent intent = new Intent(context, DetailsActivity.class);
        intent.putExtra(BundleConstant.BUNDLE_IMDB_ID, searchItem.getImdbID());
        intent.putExtra(BundleConstant.BUNDLE_NAME, searchItem.getTitle());
        intent.putExtra(BundleConstant.BUNDLE_POSTER, searchItem.getPoster());
        context.startActivity(intent);

    }


}
