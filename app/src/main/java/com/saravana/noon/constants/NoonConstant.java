package com.saravana.noon.constants;

public class NoonConstant {

    public static final String EMPTY = "";
    public static final String PRO = "PRO";
    public static final String PRO_FLAVOR = "pro";
    public static final String RELEASE = "release";

}
