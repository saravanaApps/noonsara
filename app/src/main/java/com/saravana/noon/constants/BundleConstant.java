package com.saravana.noon.constants;

public class BundleConstant {

    public static String BUNDLE_IMDB_ID = "BUNDLE_IMDB_ID";
    public static String BUNDLE_NAME = "BUNDLE_NAME";
    public static String BUNDLE_POSTER = "BUNDLE_POSTER";

}
