package com.saravana.noon.constants;

public class ApiConstants {

    public static final String CONTENT_TYPE = "content-type";
    public static final String ACCEPT_HEADER_KEY = "Accept";
    public static final String APPLICATION_JSON = "application/json";

}
