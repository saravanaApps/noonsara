package com.saravana.noon.application;

import android.app.Application;

import androidx.appcompat.app.AppCompatDelegate;

public class NoonApp extends Application {

    private static NoonApp applicationContext;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        applicationContext = this;
        AppHandler.init();
    }

    public static NoonApp getAppContext() {
        return applicationContext;
    }

    @Override
    public void onTerminate() {
        AppHandler.stopLifeCycleOwner();
        super.onTerminate();
    }

}
