package com.saravana.noon.application;

import com.saravana.noon.model.db.DBHelper;
import com.saravana.noon.model.repository.BookMarkRepo;
import com.saravana.noon.utils.NetworkUtils;
import com.saravana.noon.utils.NoonLifeCycleOwner;

public class AppHandler {

    private static NoonLifeCycleOwner lifeCycleOwner = new NoonLifeCycleOwner();

    public static void init() {

        NetworkUtils.getInstance(NoonApp.getAppContext()).register();

        DBHelper.getInstance(NoonApp.getAppContext());

        BookMarkRepo.getInstance().readAllIds(lifeCycleOwner);

    }

    public static void stopLifeCycleOwner() {
        lifeCycleOwner.stopListening();
    }


}
