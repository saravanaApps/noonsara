package com.saravana.noon.view.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import androidx.lifecycle.MutableLiveData;

import com.saravana.noon.R;
import com.saravana.noon.view.activity.BaseActivity;

public class BaseCustomView extends RelativeLayout {

    protected Context context;
    protected int aspectPadding = 0;
    protected float aspectColumn = 1;
    protected float aspectRatio = 1;
    private MutableLiveData<Object> data;
    public boolean isLifeCycleAware;

    public BaseCustomView(Context context) {
        super(context);
        initView(context, null);
    }

    public BaseCustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs);
    }

    public BaseCustomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    public BaseCustomView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context, attrs);
    }

    public void setData(Object value) {
        if (data != null) {
            if (value != null && value != data.getValue()) {
                this.data.setValue(value);
            } else if (value == null) {
                setVisibility(GONE);
            }
        } else {
            bindViewData(value);
        }
    }

    public void initView(Context context, AttributeSet attrs) {

        this.context = context;

        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.AspectAttrs);
            aspectPadding = typedArray.getInteger(R.styleable.AspectAttrs_aspect_padding, 0);
            aspectColumn = typedArray.getFloat(R.styleable.AspectAttrs_aspect_column, 1);
            aspectRatio = typedArray.getFloat(R.styleable.AspectAttrs_aspect_ratio, 1);
        }

        if (context instanceof BaseActivity) {
            isLifeCycleAware = true;
            data = new MutableLiveData<>();
            data.observe((BaseActivity) this.context, value -> {
                bindViewData(value);
            });
        } else {
            isLifeCycleAware = false;
        }

    }

    protected void bindViewData(Object value) {
    }

    public boolean isLifeCycleAware() {
        return isLifeCycleAware;
    }

}
