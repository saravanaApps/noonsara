package com.saravana.noon.view.decoration;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

public class GridListDecoration extends RecyclerView.ItemDecoration {
    private boolean isLeft, isTop, isRight, isBottom;
    private int space;
    private int columnSpan;

    public GridListDecoration(int space, boolean isTop) {
        this.space = space;
        this.isTop = isTop;
        this.isLeft = false;
        this.isRight = false;
        this.isBottom = false;
    }

    public GridListDecoration(int space, boolean isLeft, boolean isTop,
                              boolean isRight, boolean isBottom, int columnSpan) {
        this.space = space;
        this.isTop = isTop;
        this.isLeft = isLeft;
        this.isRight = isRight;
        this.isBottom = isBottom;
        this.columnSpan = columnSpan;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

        if (isTop) {
            outRect.top = space;
        }

        if (isBottom) {
            outRect.bottom = space;
        }

        if (columnSpan > 1 && isRight && isLeft) {

            int position = parent.getLayoutManager().getPosition(view) + 1;

            if (position % columnSpan == 0) {
                outRect.right = space;
                outRect.left = space / 2;
            } else if (position % columnSpan == 1) {
                outRect.right = space / 2;
                outRect.left = space;
            } else {
                outRect.right = space / 2;
                outRect.left = outRect.right;
            }

        } else {

            if (isLeft) {
                outRect.left = space;
            }

            if (isRight) {
                outRect.right = space;
            }
        }


    }
}
