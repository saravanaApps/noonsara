package com.saravana.noon.view.customview;

import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.Nullable;

import com.saravana.noon.R;
import com.saravana.noon.databinding.ItemPlaceHolderBinding;
import com.saravana.noon.utils.ViewUtils;

public class PlaceHolderView extends BaseCustomView {

    private ItemPlaceHolderBinding placeHolderBinding;
    private Point point = new Point();

    public PlaceHolderView(Context context) {
        super(context);
    }

    public PlaceHolderView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public View getRootView() {
        return placeHolderBinding.getRoot();
    }

    public void initView(Context context, AttributeSet attrs) {

        super.initView(context, attrs);

        placeHolderBinding = ItemPlaceHolderBinding.inflate(LayoutInflater.from(context), this, true);

        if (getTag() != null && getTag().equals(getResources().getString(R.string.no_border))) {
            placeHolderBinding.view.setBackgroundResource(0);
        }

        setViewAspectRatio();

    }

    private void setViewAspectRatio() {
        if (aspectRatio > 0) {
            point = ViewUtils.getPointsAspectRatio(context, aspectColumn, aspectPadding, aspectRatio);
            placeHolderBinding.view.getLayoutParams().width = point.x;
            placeHolderBinding.view.getLayoutParams().height = point.y;
        }
    }

}
