package com.saravana.noon.view.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.databinding.ViewDataBinding;

import com.google.android.material.snackbar.Snackbar;
import com.saravana.noon.R;
import com.saravana.noon.utils.ViewUtils;
import com.saravana.noon.viewmodel.AppVM;

public abstract class BaseActivity extends AppCompatActivity {

    protected AppVM mAppVM;
    protected ImageView imgMenu;
    protected ImageView imgSearch;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        ViewUtils.updateDisplayPoint(this);

        super.onCreate(savedInstanceState);

        setContentBindingView();

        addListener();

        subscribeObserver();

    }

    private void addListener() {

        if (imgMenu != null) {
            imgMenu.setOnClickListener(v -> {
                if (imgMenu != null) {
                    if (imgMenu.getTag() != null) {
                        if (imgMenu.getTag().toString().equalsIgnoreCase(getString(R.string.menu_back_arrow))) {
                            onBackPressed();
                        }
                    }
                }
            });
        }

        if (imgSearch != null) {
            imgSearch.setOnClickListener(v -> {

            });
        }

    }

    private void subscribeObserver() {

    }

    protected AppVM createAppVM(String key) {
        return AppVM.getViewModel(this, key);
    }

    public void onNetworkRetry() {
    }

    protected abstract void setContentBindingView();

    public abstract ViewDataBinding getDataBinding();

    public abstract AppVM getViewModel();

    public abstract CoordinatorLayout getCoordinatorLayout();

    public void showSnackBar(String message) {

        if (TextUtils.isEmpty(message) || getCoordinatorLayout() == null) {
            return;
        }

        Snackbar snackbar = Snackbar.make(getCoordinatorLayout(), message, Snackbar.LENGTH_LONG);
        snackbar.show();

    }

}
