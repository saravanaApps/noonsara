package com.saravana.noon.view.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

import com.saravana.noon.R;
import com.saravana.noon.databinding.ItemAspectImageBinding;
import com.saravana.noon.model.network.PicassoTrustAll;
import com.saravana.noon.utils.ViewUtils;

public class AspectImageView extends BaseCustomView {

    private ItemAspectImageBinding imageBinding;
    private Point point = new Point();
    private int backGroundColor;

    public AspectImageView(Context context) {
        super(context);
    }

    public AspectImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public View getRootView() {
        return imageBinding.getRoot();
    }

    public void setAspectStyleable(float aspectColumn, int aspectPadding, float aspectRatio) {
        this.aspectColumn = aspectColumn;
        this.aspectPadding = aspectPadding;
        this.aspectRatio = aspectRatio;
        setImageAspectRatio();
    }

    public void initView(Context context, AttributeSet attrs) {

        super.initView(context, attrs);

        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.AspectImage);
            setImageBackgroundColor(typedArray.getResourceId(R.styleable.AspectImage_image_background_color, android.R.color.transparent));
        }

        imageBinding = ItemAspectImageBinding.inflate(LayoutInflater.from(context), this, true);

        setImageAspectRatio();

    }

    public void setImageBackgroundColor(int color) {
        backGroundColor = color;
    }

    public int getImageBackgroundColor() {
        return backGroundColor;
    }

    private void setImageAspectRatio() {
        if (aspectRatio > 0) {
            point = ViewUtils.getPointsAspectRatio(context, aspectColumn, aspectPadding, aspectRatio);
            imageBinding.imageView.getLayoutParams().width = point.x;
            imageBinding.imageView.getLayoutParams().height = point.y;
        }
    }

    public Point getDisplayPoint() {
        return point;
    }

    public void loadImage(String imageUrl) {
        imageBinding.imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        PicassoTrustAll.getInstance(context).loadImage(imageBinding.imageView, imageUrl, context);
    }

    public AppCompatImageView getImageView() {
        return imageBinding.imageView;
    }


}
