package com.saravana.noon.view.customview;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.saravana.noon.model.datamodel.SearchItem;
import com.saravana.noon.model.datamodel.SearchItemResponse;
import com.saravana.noon.model.datamodel.SearchReq;
import com.saravana.noon.model.network.Resource;
import com.saravana.noon.utils.ViewUtils;
import com.saravana.noon.view.activity.BaseActivity;
import com.saravana.noon.view.adapter.SearchItemViewAdapter;
import com.saravana.noon.view.decoration.GridListDecoration;

import java.util.ArrayList;
import java.util.List;

public class SearchList extends RecyclerView {

    private boolean isLoading = false;

    private SearchItemResponse response;
    private SearchReq searchReq;

    private SearchItemViewAdapter adapter;
    private BaseActivity baseActivity;
    private int mCount = 1;

    public SearchList(@NonNull Context context) {
        super(context);
    }

    public SearchList(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public SearchList(@NonNull Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void init(BaseActivity baseActivity) {

        this.baseActivity = baseActivity;

        addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {

                try {

                    super.onScrolled(recyclerView, dx, dy);

                    if (response == null ||
                            response.getViewType() == ViewUtils.VIEW_SEARCH_SUGGESTION ||
                            isRecordCountOver()) {
                        return;
                    }

                    if (dy > 0 && getLayoutManager() != null) {

                        int visibleItemCount = getLayoutManager().getChildCount();
                        int totalItemCount = getLayoutManager().getItemCount();
                        int pastVisibleItems = ((GridLayoutManager) getLayoutManager()).findFirstVisibleItemPosition();

                        if (response != null && !isLoading && !isRecordCountOver() && (visibleItemCount + pastVisibleItems) >= (totalItemCount - 6)) {
                            isLoading = true;
                            searchReq.setPage(mCount += 1);
                            loadSearchData(searchReq);
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

        });

        if (this.baseActivity == null) {
            return;
        }

        baseActivity.getViewModel().getSearchResults().observe(baseActivity, res -> {

            try {

                if (res == null) {
                    return;
                }

                if (res.status == Resource.APIStatus.SUCCESS && res.data != null) {

                    response = res.data;
                    isLoading = false;
                    mCount = response.getPageNo();
                    setSearchAdapter(response.getSearch());

                } else if (res.status == Resource.APIStatus.ERROR) {
                    isLoading = false;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        });

    }

    private void setSearchAdapter(List<SearchItem> data) {

        if (data == null || data.isEmpty()) {
            setVisibility(View.GONE);
            return;
        }

        try {
            if (!response.getSearchQuery().equals(searchReq.getSearchQuery())) {
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        setVisibility(View.VISIBLE);

        if (adapter == null || getAdapter() == null) {

            adapter = new SearchItemViewAdapter(baseActivity);

            GridLayoutManager layoutManager = new GridLayoutManager(baseActivity, 2, GridLayoutManager.VERTICAL, false) {
                @Override
                public boolean canScrollHorizontally() {
                    return false;
                }

                @Override
                public boolean canScrollVertically() {
                    return true;
                }

            };

            setLayoutManager(layoutManager);

            addItemDecoration(new GridListDecoration(ViewUtils.dpToPixel(5, baseActivity),
                    true, false, true, true, layoutManager.getSpanCount()));

            setAdapter(adapter);

        }

        boolean isRefresh = (response != null && response.getPageNo() <= 1);
        adapter.setData(data, isRefresh);

        if (isRefresh) {
            scrollToPosition(0);
        }

    }

    public void loadSearchData(SearchReq searchReq) {

        if (searchReq == null || baseActivity == null) {
            return;
        }

        this.searchReq = searchReq;
        baseActivity.getViewModel().setSearchRequest(searchReq);

    }

    public void loadDefaultSearchData(String query) {

        if (TextUtils.isEmpty(query)) {
            return;
        }

        setSearchAdapter(getLoaderData(4));

        searchReq = new SearchReq();
        searchReq.setSearchQuery(query);
        searchReq.setViewType(ViewUtils.VIEW_SEARCH_SUGGESTION);

        loadSearchData(searchReq);

    }

    public void loadContentDirectly(SearchItemResponse res) {
        try {

            if (res == null) {
                setSearchAdapter(null);
                return;
            }

            searchReq = new SearchReq();
            searchReq.setSearchQuery(res.getSearchQuery());
            searchReq.setViewType(res.getViewType());

            response = res;
            isLoading = false;
            mCount = response.getPageNo();
            setSearchAdapter(response.getSearch());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isRecordCountOver() {

        if (response != null) {
            return response.isRecordSetOver();
        }

        return false;

    }

    private ArrayList<SearchItem> getLoaderData(int count) {
        ArrayList<SearchItem> list = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            SearchItem searchItem = new SearchItem();
            searchItem.setViewType(ViewUtils.VIEW_LOADER);
            list.add(searchItem);
        }
        return list;
    }

}
