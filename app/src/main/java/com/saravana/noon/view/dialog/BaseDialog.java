package com.saravana.noon.view.dialog;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.ViewGroup;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import com.saravana.noon.R;

public abstract class BaseDialog extends DialogFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getTheme() {
        return R.style.dialog_fragment_style;
    }

    @Override
    public void onStart() {
        super.onStart();
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        Double width = metrics.widthPixels * .9;
        getDialog().getWindow().setLayout(width.intValue(), ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    public void closeDialog() {
        try {
            if (!this.isRemoving()) {
                dismiss();
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        setRetainInstance(true);
        super.show(manager, tag);
    }

}
