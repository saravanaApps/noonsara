package com.saravana.noon.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.saravana.noon.R;
import com.saravana.noon.constants.BundleConstant;
import com.saravana.noon.databinding.ActivityDetailsBinding;
import com.saravana.noon.databinding.ItemDetailListBinding;
import com.saravana.noon.databinding.ItemRatingListBinding;
import com.saravana.noon.model.datamodel.ContentDetails;
import com.saravana.noon.model.datamodel.Ratings;
import com.saravana.noon.model.datamodel.SearchReq;
import com.saravana.noon.model.network.Resource;
import com.saravana.noon.model.repository.BookMarkRepo;
import com.saravana.noon.utils.NavUtil;
import com.saravana.noon.viewmodel.AppVM;

import java.util.Date;

public class DetailsActivity extends BaseActivity {

    private ActivityDetailsBinding binding;
    private ContentDetails contentDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        mAppVM = createAppVM(String.valueOf(new Date().getTime()));

        super.onCreate(savedInstanceState);

        setSupportActionBar(binding.toolbar);

        subscribeUi();

        getData();

    }

    private void getData() {

        if (getIntent() == null || getIntent().getStringExtra(BundleConstant.BUNDLE_IMDB_ID) == null) {
            return;
        }

        SearchReq searchReq = new SearchReq();
        searchReq.setImdbId(getIntent().getStringExtra(BundleConstant.BUNDLE_IMDB_ID));
        searchReq.setPlot(getString(R.string.plot_full));

        getViewModel().setDetailRequest(searchReq);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);

    }

    @Override
    public void setContentBindingView() {

        binding = DataBindingUtil.setContentView(this, R.layout.activity_details);
        imgMenu = binding.actionHeader.imgBackArrow;

        binding.layPlaceHolder.startShimmerAnimation();

        if (getIntent() != null) {
            if (getIntent().getStringExtra(BundleConstant.BUNDLE_NAME) != null) {
                binding.actionHeader.txtHeader.setText(getIntent().getStringExtra(BundleConstant.BUNDLE_NAME).trim());
            }
        }

        binding.actionHeader.imgSearch.setOnClickListener(v -> NavUtil.getNavUtils(DetailsActivity.this).openSearchActivity());

        binding.actionHeader.imgBookmark.setOnClickListener(v -> NavUtil.getNavUtils(DetailsActivity.this).openBookMarkActivity());

        binding.viewBookmark.setOnClickListener(v -> binding.imgBookmark.updateBookMark());

    }

    private void updateBookmarkText(boolean isBookMarked) {

        if (contentDetails == null) {
            return;
        }

        if (isBookMarked) {
            binding.txtBookMark.setText(getString(R.string.remove_bookmark));
        } else {
            binding.txtBookMark.setText(getString(R.string.bookmark));
        }

    }

    private void subscribeUi() {

        BookMarkRepo.getInstance().readAllIds().observe(this, res -> {
            try {
                if (res != null) {
                    String key = contentDetails.getImdbID();
                    updateBookmarkText(!TextUtils.isEmpty(key) && res.contains(key));
                } else {
                    updateBookmarkText(false);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        getViewModel().getDetailResults().observe(this, res -> {

            if (res.status == Resource.APIStatus.LOADING) {
                binding.layPlaceHolder.setVisibility(View.VISIBLE);
                binding.layPlaceHolder.startShimmerAnimation();
                binding.viewContentHolder.setVisibility(View.GONE);
                return;
            }

            if (res.status == Resource.APIStatus.SUCCESS && res.data != null) {

                binding.layPlaceHolder.setVisibility(View.GONE);
                binding.viewContentHolder.setVisibility(View.VISIBLE);

                contentDetails = res.data;

                renderUI();

            } else if (res.status == Resource.APIStatus.ERROR) {

                binding.layPlaceHolder.setVisibility(View.GONE);
                binding.viewContentHolder.setVisibility(View.GONE);

                showSnackBar(res.message);

            }

        });

    }

    private void renderUI() {

        try {

            if (contentDetails == null) {
                return;
            }

            binding.imgPoster.loadImage(contentDetails.getPoster());

            binding.imgBookmark.setData(contentDetails.getSearchItemObj());

            updateBookmarkText(BookMarkRepo.getInstance().isContainBookMark(contentDetails.getImdbID()));

            binding.txtTitle.setText(contentDetails.getTitle());
            binding.txtDirector.setText(contentDetails.getDirector());
            binding.txtActors.setText(contentDetails.getActors());
            binding.txtType.setText(contentDetails.getTypeDuration());
            binding.txtImdbRating.setText(contentDetails.getImdbRatingVoting());
            binding.txtPlot.setText(contentDetails.getPlot());

            //Adding Rating Views at Runtime
            if (contentDetails.getRatings() == null || contentDetails.getRatings().size() == 0) {

                binding.txtHeaderRating.setVisibility(View.GONE);
                binding.viewRatings.setVisibility(View.GONE);

            } else {

                binding.txtHeaderRating.setVisibility(View.VISIBLE);
                binding.viewRatings.setVisibility(View.VISIBLE);

                binding.viewRatings.removeAllViews();

                for (Ratings ratings : contentDetails.getRatings()) {
                    ItemRatingListBinding listBinding = ItemRatingListBinding.inflate(getLayoutInflater(), null, true);
                    listBinding.txtRating.setText(ratings.getSource());
                    listBinding.txtRatingValue.setText(ratings.getValue());
                    binding.viewRatings.addView(listBinding.getRoot());
                }

            }

            //Adding More Information Views at Runtime
            if (contentDetails.getMoreInformation() == null || contentDetails.getMoreInformation().size() == 0) {

                binding.txtHeaderDetails.setVisibility(View.GONE);
                binding.viewDetails.setVisibility(View.GONE);

            } else {

                binding.txtHeaderDetails.setVisibility(View.VISIBLE);
                binding.viewDetails.setVisibility(View.VISIBLE);

                binding.viewDetails.removeAllViews();

                for (String key : contentDetails.getMoreInformation().keySet()) {
                    ItemDetailListBinding listBinding = ItemDetailListBinding.inflate(getLayoutInflater(), null, true);
                    listBinding.txtDetailTitle.setText(key);
                    listBinding.txtDetailValue.setText(contentDetails.getMoreInformation().get(key));
                    binding.viewDetails.addView(listBinding.getRoot());
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public ViewDataBinding getDataBinding() {
        return binding;
    }

    @Override
    public AppVM getViewModel() {
        return mAppVM;
    }

    @Override
    public CoordinatorLayout getCoordinatorLayout() {
        return binding.viewContent;
    }

}
