package com.saravana.noon.view.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.saravana.noon.R;
import com.saravana.noon.application.NoonApp;
import com.saravana.noon.databinding.LayoutDialogNetworkBinding;
import com.saravana.noon.utils.NetworkUtils;
import com.saravana.noon.view.activity.AppOpeningActivity;
import com.saravana.noon.view.activity.BaseActivity;

public class NetworkDialog extends com.saravana.noon.view.dialog.BaseDialog implements View.OnClickListener {

    private LayoutDialogNetworkBinding binding;

    public static NetworkDialog newInstance() {
        NetworkDialog fragment = new NetworkDialog();
        return fragment;
    }

    public static NetworkDialog newInstance(Bundle bundle) {
        NetworkDialog fragment = new NetworkDialog();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void onClickBtnAction() {

        if (!NetworkUtils.getInstance(NoonApp.getAppContext()).isNetworkAvailable()) {
            return;
        }

        try {

            if (getActivity() instanceof BaseActivity) {
                ((BaseActivity) getActivity()).onNetworkRetry();
            } else if (getActivity() instanceof AppOpeningActivity) {
                ((AppOpeningActivity) getActivity()).onNetworkRetry();
            }

            closeDialog();

        } catch (Exception e) {
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAction:
                onClickBtnAction();
                break;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawableResource(R.drawable.dialog_background);
        }

        binding = LayoutDialogNetworkBinding.inflate(LayoutInflater.from(getContext()), container, true);
        binding.btnAction.setOnClickListener(this::onClick);

        setCancelable(false);

        return binding.getRoot();

    }

    @Override
    public void closeDialog() {
        super.closeDialog();
    }

}
