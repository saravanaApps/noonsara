package com.saravana.noon.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.saravana.noon.R;
import com.saravana.noon.databinding.ItemSearchFooterBinding;
import com.saravana.noon.databinding.ItemSearchViewBinding;
import com.saravana.noon.databinding.ViewSearchItemLoaderBinding;
import com.saravana.noon.model.datamodel.SearchItem;
import com.saravana.noon.utils.ViewUtils;
import com.saravana.noon.view.activity.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class SearchItemViewAdapter extends BaseAdapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    private List<SearchItem> mSearchItemList;

    public SearchItemViewAdapter(BaseActivity baseActivity) {
        super(baseActivity);
        mSearchItemList = new ArrayList<>();
    }

    public void setData(List<SearchItem> searchItems, boolean isRefresh) {

        int currentItemCount = getItemCount();

        mSearchItemList.clear();
        mSearchItemList.addAll(searchItems);

        if (isRefresh || currentItemCount == 0) {
            notifyDataSetChanged();
        } else if (currentItemCount < getItemCount()) {
            notifyItemRangeChanged(currentItemCount, getItemCount());
        } else {
            notifyDataSetChanged();
        }

        /* final SearchDiffCallback diffCallback = new SearchDiffCallback(mSearchItemList, searchItems);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        mSearchItemList.clear();
        mSearchItemList.addAll(searchItems);

        diffResult.dispatchUpdatesTo(SearchItemViewAdapter.this); */

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == ViewUtils.VIEW_SEARCH_ITEM) {

            ItemSearchViewBinding viewHolder = ItemSearchViewBinding.inflate(
                    LayoutInflater.from(context), parent, false);
            return new SearchItemHolder(viewHolder);

        } else if (viewType == ViewUtils.VIEW_LOADER) {

            ViewSearchItemLoaderBinding viewHolder = ViewSearchItemLoaderBinding.inflate(
                    LayoutInflater.from(context), parent, false);
            return new ViewHolderLoader(viewHolder);

        } else if (viewType == ViewUtils.VIEW_FOOTER) {

            ItemSearchFooterBinding viewHolder = ItemSearchFooterBinding.inflate(
                    LayoutInflater.from(context), parent, false);
            return new ViewHolderFooter(viewHolder);

        }

        throw new IllegalArgumentException("Invalid View Type : " + viewType);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        int viewType = getItemViewType(position);

        switch (viewType) {
            case ViewUtils.VIEW_SEARCH_ITEM:
                setSearchItemView(holder, position);
                break;

            case ViewUtils.VIEW_LOADER:
                ((ViewHolderLoader) holder).binding.layPlaceHolder.startShimmerAnimation();
                break;

            case ViewUtils.VIEW_FOOTER:
                setFooterData(holder, position);
                break;

        }

    }

    private void setSearchItemView(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (mSearchItemList == null || mSearchItemList.size() <= position || !(holder instanceof SearchItemHolder)) {
            holder.itemView.setVisibility(View.GONE);
            return;
        }

        SearchItem searchItem = mSearchItemList.get(position);

        if (searchItem == null) {
            holder.itemView.setVisibility(View.GONE);
            return;
        }

        holder.itemView.setVisibility(View.VISIBLE);
        SearchItemHolder viewHolder = (SearchItemHolder) holder;

        viewHolder.binding.txtTitle.setText(searchItem.getTitle());
        viewHolder.binding.imgPoster.loadImage(searchItem.getPoster());

        viewHolder.binding.cardView.setTag(R.string.view_tag, searchItem);
        viewHolder.binding.cardView.setOnClickListener(this);

        viewHolder.binding.imgBookmark.setData(searchItem);


    }

    private void setFooterData(RecyclerView.ViewHolder holder, int position) {

        if (mSearchItemList == null || mSearchItemList.size() <= position ||
                !(holder instanceof ViewHolderFooter)) {
            holder.itemView.setVisibility(View.GONE);
            return;
        }

        holder.itemView.setVisibility(View.VISIBLE);

        ViewHolderFooter holderFooter = (ViewHolderFooter) holder;
        SearchItem searchItem = mSearchItemList.get(position);

        holderFooter.binding.txtHeader.setText(searchItem.getTitle());

    }

    @Override
    public int getItemCount() {
        return mSearchItemList != null ? mSearchItemList.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {

        if (mSearchItemList == null || mSearchItemList.size() <= position) {
            return ViewUtils.VIEW_LOADER;
        }

        return mSearchItemList.get(position).getViewType();

    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.card_view) {
            updateSelectedItem(v);
        }

    }

    private void updateSelectedItem(View v) {
        try {
            if (v != null && v.getTag(R.string.view_tag) != null) {
                ((BaseActivity) context).getViewModel().setSelectedSearchItem(
                        (SearchItem) v.getTag(R.string.view_tag));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static class SearchItemHolder extends RecyclerView.ViewHolder {

        private ItemSearchViewBinding binding;

        SearchItemHolder(ItemSearchViewBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }
    }

    static class ViewHolderLoader extends RecyclerView.ViewHolder {

        private ViewSearchItemLoaderBinding binding;

        ViewHolderLoader(ViewSearchItemLoaderBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
            binding.layPlaceHolder.startShimmerAnimation();
        }
    }

    static class ViewHolderFooter extends RecyclerView.ViewHolder {

        private ItemSearchFooterBinding binding;

        ViewHolderFooter(ItemSearchFooterBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }
    }

}
