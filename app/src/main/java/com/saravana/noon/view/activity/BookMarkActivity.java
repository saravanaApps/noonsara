package com.saravana.noon.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.saravana.noon.R;
import com.saravana.noon.constants.NoonConstant;
import com.saravana.noon.databinding.ActivityBookmarkBinding;
import com.saravana.noon.model.datamodel.SearchItemResponse;
import com.saravana.noon.model.repository.BookMarkRepo;
import com.saravana.noon.utils.NavUtil;
import com.saravana.noon.utils.ViewUtils;
import com.saravana.noon.viewmodel.AppVM;

import java.util.Collections;
import java.util.Date;

public class BookMarkActivity extends BaseActivity {

    private static final String TAG = BookMarkActivity.class.getSimpleName();

    private ActivityBookmarkBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        mAppVM = createAppVM(String.valueOf(new Date().getTime()));

        super.onCreate(savedInstanceState);

        setSupportActionBar(binding.toolbar);

        subscribeUi();

        binding.rvSearchList.loadDefaultSearchData(getString(R.string.default_search_key));

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    public void setContentBindingView() {

        binding = DataBindingUtil.setContentView(this, R.layout.activity_bookmark);
        imgMenu = binding.actionHeader.imgBackArrow;

        binding.actionHeader.txtHeader.setText(getString(R.string.bookmark));

        binding.actionHeader.imgSearch.setOnClickListener(v -> NavUtil.getNavUtils(BookMarkActivity.this).openSearchActivity());

        binding.actionHeader.imgBookmark.setImageDrawable(getDrawable(R.drawable.vec_delete));
        binding.actionHeader.imgBookmark.setOnClickListener(v -> BookMarkRepo.getInstance().deleteAll());

        binding.rvSearchList.init(this);

    }

    @Override
    public ViewDataBinding getDataBinding() {
        return binding;
    }

    @Override
    public AppVM getViewModel() {
        return mAppVM;
    }

    @Override
    public CoordinatorLayout getCoordinatorLayout() {
        return binding.viewContent;
    }

    private void subscribeUi() {

        BookMarkRepo.getInstance().readAllData().observe(this, res -> {

            if (binding.rvSearchList.getAdapter() != null) {

                if (res == null || res.size() == 0) {
                    binding.actionHeader.txtHeader.setText(getString(R.string.bookmark));
                    binding.rvSearchList.setVisibility(View.GONE);
                    binding.txtError.setVisibility(View.VISIBLE);
                    return;
                }

                binding.txtError.setVisibility(View.GONE);
                binding.rvSearchList.setVisibility(View.VISIBLE);

                SearchItemResponse searchItemResponse = new SearchItemResponse();
                searchItemResponse.setViewType(ViewUtils.VIEW_SEARCH_BOOKMARK);
                searchItemResponse.setRecordSetOver(true);
                searchItemResponse.setPageNo(res.size() / 10);
                searchItemResponse.setTotalResults(res.size() + NoonConstant.EMPTY);
                searchItemResponse.setSearchQuery(getString(R.string.bookmark));

                Collections.reverse(res);

                searchItemResponse.setSearch(res);

                binding.actionHeader.txtHeader.setText(getString(R.string.bookmark_count,
                        searchItemResponse.getTotalResultsInStr()));

                binding.rvSearchList.loadContentDirectly(searchItemResponse);

            }

        });

        getViewModel().getSelectedSearchItem().observe(this, searchItem ->
                NavUtil.getNavUtils(BookMarkActivity.this).openDetailsActivity(searchItem));

    }

}
