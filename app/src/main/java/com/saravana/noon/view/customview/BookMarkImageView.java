package com.saravana.noon.view.customview;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

import com.saravana.noon.R;
import com.saravana.noon.model.datamodel.SearchItem;
import com.saravana.noon.model.repository.BookMarkRepo;
import com.saravana.noon.view.activity.BaseActivity;
import com.saravana.noon.view.activity.SearchActivity;

public class BookMarkImageView extends AppCompatImageView {

    private SearchItem data;

    public BookMarkImageView(Context context) {
        super(context);
        initView(context);
    }

    public BookMarkImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public BookMarkImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {

        BookMarkRepo.getInstance().readAllIds().observe((BaseActivity) context, res -> {
            try {
                if (res != null) {
                    String key = getKeyValue();
                    updateBookMarkView(!TextUtils.isEmpty(key) && res.contains(key));
                } else {
                    updateBookMarkView(false);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        setOnClickListener(v -> {
            if (isClickable()) {
                updateBookMark();
            }
        });

    }

    public void setData(SearchItem data) {
        this.data = data;
        if (data != null) {
            updateBookMarkView(BookMarkRepo.getInstance().isContainBookMark(data.getImdbID()));
        }
    }

    public SearchItem getData() {
        return data;
    }

    public String getKeyValue() {
        if (data != null) {
            return data.getImdbID();
        }
        return null;
    }

    public void updateBookMark() {
        try {

            if (data == null) {
                return;
            }

            String msg = null;

            if (BookMarkRepo.getInstance().isContainBookMark(data)) {
                BookMarkRepo.getInstance().removeItem(data);
                msg = getContext().getString(R.string.removed_bookmark_msg, data.getTitle());
            } else {
                msg = getContext().getString(R.string.added_bookmark_msg, data.getTitle());
                BookMarkRepo.getInstance().addItem(data);
            }

            if (getContext() instanceof BaseActivity) {
                ((BaseActivity) getContext()).showSnackBar(msg);
            }

        } catch (
                Exception e) {
            e.printStackTrace();
        }

    }

    private void updateBookMarkView(boolean isBookMarked) {
        try {
            if (isBookMarked) {
                setImageDrawable(getContext().getDrawable(R.drawable.vec_bookmark));
            } else {
                setImageDrawable(getContext().getDrawable(R.drawable.vec_un_bookmark));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
