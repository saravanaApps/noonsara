package com.saravana.noon.view.utils;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;

import com.saravana.noon.model.datamodel.SearchItem;

import java.util.List;

public class SearchDiffCallback extends DiffUtil.Callback {

    private final List<SearchItem> mOldSearchList;
    private final List<SearchItem> mNewSearchList;

    public SearchDiffCallback(List<SearchItem> oldSearchList, List<SearchItem> newSearchList) {
        this.mOldSearchList = oldSearchList;
        this.mNewSearchList = newSearchList;
    }

    @Override
    public int getOldListSize() {
        return mOldSearchList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewSearchList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        try {
            return mOldSearchList.get(oldItemPosition).getImdbID() == mNewSearchList.get(newItemPosition).getImdbID();
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        try {
            final SearchItem oldSearch = mOldSearchList.get(oldItemPosition);
            final SearchItem newSearch = mNewSearchList.get(newItemPosition);
            return oldSearch.getTitle().equals(newSearch.getTitle());
        } catch (Exception e) {
            return false;
        }
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        // Implement method if you're going to use ItemAnimator
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }
}
