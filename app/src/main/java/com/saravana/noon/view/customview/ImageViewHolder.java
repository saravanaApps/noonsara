package com.saravana.noon.view.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;

import androidx.annotation.Nullable;

import com.saravana.noon.R;
import com.saravana.noon.databinding.VhImageBinding;

public class ImageViewHolder extends BaseCustomView {

    private VhImageBinding imageBinding;
    private String targetUrl;
    private int backGroundColor;

    public ImageViewHolder(Context context) {
        super(context);
    }

    public ImageViewHolder(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ImageViewHolder(Context context, int backGroundColor) {
        super(context);
        this.backGroundColor = backGroundColor;
    }

    public void setAspectStyleable(float aspectColumn, int aspectPadding, float aspectRatio) {
        this.aspectColumn = aspectColumn;
        this.aspectPadding = aspectPadding;
        this.aspectRatio = aspectRatio;
        imageBinding.imgView.setAspectStyleable(aspectColumn, aspectPadding, aspectRatio);
        resizeCardView();
    }

    public void initView(Context context, AttributeSet attrs) {

        super.initView(context, attrs);

        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.AspectImage);
            backGroundColor = typedArray.getResourceId(R.styleable.AspectImage_image_background_color, android.R.color.transparent);
        }

        imageBinding = VhImageBinding.inflate(LayoutInflater.from(context), this, true);

        if (aspectRatio > 0) {
            imageBinding.imgView.setAspectStyleable(aspectColumn, aspectPadding, aspectRatio);
            resizeCardView();
        }

    }

    private void resizeCardView() {
        imageBinding.cardView.getLayoutParams().width = imageBinding.imgView.getDisplayPoint().x;
        imageBinding.cardView.getLayoutParams().height = imageBinding.imgView.getDisplayPoint().y;
    }

    public void setTargetUrl(String targetUrl) {
        this.targetUrl = targetUrl;
        imageBinding.cardView.setClickable(!TextUtils.isEmpty(targetUrl));
    }

    public void loadImage(int viewType, String imageUrl) {
        setTargetUrl(targetUrl);
        imageBinding.imgView.loadImage(imageUrl);
    }

}
