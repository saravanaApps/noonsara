package com.saravana.noon.view.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.saravana.noon.application.NoonApp;
import com.saravana.noon.utils.NavUtil;
import com.saravana.noon.utils.NetworkUtils;
import com.saravana.noon.view.dialog.NetworkDialog;
import com.saravana.noon.viewmodel.AppVM;

public class AppOpeningActivity extends AppCompatActivity {

    private boolean isNetworkAvailable;
    protected AppVM mViewModel;
    protected NetworkDialog networkDialog;
    protected boolean isStatus = false;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        mViewModel = AppVM.getViewModel(this, AppOpeningActivity.class.getSimpleName());

        NetworkUtils.getInstance(NoonApp.getAppContext()).getNetworkLiveData().observe(this, value -> {
            isNetworkAvailable = value;
            if (!isNetworkAvailable) {
                NavUtil.getNavUtils(AppOpeningActivity.this).onNetworkStatusChange(AppOpeningActivity.this, networkDialog, value);
            } else {
                checkStatus();
            }
        });

    }

    private void checkStatus() {
        if (isNetworkAvailable && !isStatus) {
            isStatus = true;
            NavUtil.getNavUtils(this).openSearchActivity();
        }
    }

    public void onNetworkRetry() {
        isNetworkAvailable = NetworkUtils.getInstance(this).isNetworkAvailable();
        checkStatus();
    }

}
