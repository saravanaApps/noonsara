package com.saravana.noon.view.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.saravana.noon.R;
import com.saravana.noon.databinding.ActivitySearchBinding;
import com.saravana.noon.model.datamodel.SearchReq;
import com.saravana.noon.model.network.Resource;
import com.saravana.noon.utils.NavUtil;
import com.saravana.noon.utils.ViewUtils;
import com.saravana.noon.viewmodel.AppVM;

import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class SearchActivity extends BaseActivity {

    private static final String TAG = SearchActivity.class.getSimpleName();

    public static final int DELAYED_PROMPT = 2000;
    public static final int REQ_CODE_SPEECH_INPUT = 101;

    private ActivitySearchBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        mAppVM = createAppVM(String.valueOf(new Date().getTime()));

        super.onCreate(savedInstanceState);

        setSupportActionBar(binding.toolbar);

        subscribeUi();

        binding.rvSearchList.loadDefaultSearchData(getString(R.string.default_search_key));

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    public void setContentBindingView() {

        binding = DataBindingUtil.setContentView(this, R.layout.activity_search);
        imgMenu = binding.actionHeader.imgBackArrow;

        binding.rvSearchList.init(this);

        binding.actionHeader.imgSearch.setOnClickListener(v -> {

            if (binding.actionHeader.edtSearch.getVisibility() == View.VISIBLE) {

                searchQuery(binding.actionHeader.edtSearch.getText().toString().trim());

            } else {

                binding.actionHeader.edtSearch.setVisibility(View.VISIBLE);
                binding.actionHeader.imgClose.setVisibility(View.VISIBLE);

                binding.actionHeader.txtHeader.setVisibility(View.GONE);
                binding.actionHeader.imgSpeaker.setVisibility(View.GONE);

                binding.actionHeader.edtSearch.requestFocus();
                ViewUtils.showSoftKeyboard(this, binding.actionHeader.edtSearch, 500);

            }

        });

        binding.actionHeader.imgClose.setOnClickListener(v -> binding.actionHeader.edtSearch.getText().clear());

        binding.actionHeader.imgSpeaker.setOnClickListener(v -> promptSpeechInput());

        binding.actionHeader.edtSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                searchQuery(binding.actionHeader.edtSearch.getText().toString().trim());
                return true;
            }
            return false;
        });

        binding.actionHeader.edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                String query = binding.actionHeader.edtSearch.getText().toString().trim();

                if (TextUtils.isEmpty(query)) {
                    binding.rvSearchList.loadSearchData(new SearchReq(getString(R.string.default_search_key)));
                } else {
                    binding.rvSearchList.loadSearchData(new SearchReq(query));
                }

            }
        });

    }

    private void searchQuery(String value) {

        ViewUtils.hideSoftKeyboard(binding.actionHeader.edtSearch, this);

        binding.actionHeader.edtSearch.setVisibility(View.GONE);
        binding.actionHeader.imgClose.setVisibility(View.GONE);

        binding.actionHeader.txtHeader.setVisibility(View.VISIBLE);
        binding.actionHeader.imgSpeaker.setVisibility(View.VISIBLE);

        if (TextUtils.isEmpty(value)) {
            binding.actionHeader.txtHeader.setText(getString(R.string.label_search));
            binding.rvSearchList.loadDefaultSearchData(getString(R.string.default_search_key));
            return;
        }

        binding.actionHeader.txtHeader.setText(value);

        SearchReq searchReq = new SearchReq();
        searchReq.setSearchQuery(value);
        searchReq.setViewType(ViewUtils.VIEW_SEARCH_RESULTS);

        binding.rvSearchList.loadSearchData(searchReq);


    }

    private void subscribeUi() {

        getViewModel().getSearchResults().observe(this, res -> {

            try {

                if (res == null) {
                    return;
                }

                if (res.status == Resource.APIStatus.SUCCESS && res.data != null) {

                    if (res.data.getViewType() == ViewUtils.VIEW_SEARCH_RESULTS) {
                        binding.actionHeader.txtHeader.setText(getString(R.string.search_title,
                                res.data.getSearchQuery(), res.data.getTotalResultsInStr()));
                    }

                } else if (res.status == Resource.APIStatus.ERROR) {

                    if (res.data != null && res.data.getViewType() == ViewUtils.VIEW_SEARCH_SUGGESTION) {
                        return;
                    }

                    showSnackBar(res.message);

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        });

        getViewModel().getSelectedSearchItem().observe(this, searchItem -> NavUtil.getNavUtils(SearchActivity.this).openDetailsActivity(searchItem));

    }

    public void promptSpeechInput() {

        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS, DELAYED_PROMPT);
        intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_POSSIBLY_COMPLETE_SILENCE_LENGTH_MILLIS, DELAYED_PROMPT);
        intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_MINIMUM_LENGTH_MILLIS, DELAYED_PROMPT);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, getString(R.string.speech_prompt));

        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Log.e(TAG, "promptSpeechInput: speech_not_supported");
        }

    }

    @Override
    public ViewDataBinding getDataBinding() {
        return binding;
    }

    @Override
    public AppVM getViewModel() {
        return mAppVM;
    }

    @Override
    public CoordinatorLayout getCoordinatorLayout() {
        return binding.viewContent;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_CODE_SPEECH_INPUT) {
            if (resultCode == RESULT_OK && null != data) {
                ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                if (!result.isEmpty() && !TextUtils.isEmpty(result.get(0))) {
                    binding.actionHeader.txtHeader.setText(result.get(0));
                    searchQuery(result.get(0).trim());
                }
            }
        }
    }

}
