package com.saravana.noon.view.decoration;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

public class ListDecoration extends RecyclerView.ItemDecoration {
    private boolean isLeft, isTop, isRight, isBottom;
    private int space;

    public ListDecoration(int space, boolean isTop) {
        this.space = space;
        this.isTop = isTop;
        this.isLeft = false;
        this.isRight = false;
        this.isBottom = false;
    }

    public ListDecoration(int space, boolean isLeft, boolean isTop, boolean isRight, boolean isBottom) {
        this.space = space;
        this.isTop = isTop;
        this.isLeft = isLeft;
        this.isRight = isRight;
        this.isBottom = isBottom;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

        if (isTop) {
            outRect.top = space;
        }

        if (isLeft) {
            outRect.left = space;
        }

        if (isRight) {
            outRect.right = space;
        }

        if (isBottom) {
            outRect.bottom = space;
        }

    }
}
